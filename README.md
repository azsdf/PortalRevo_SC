### 传送门：革命 简体中文补丁

---

![Portal: Revolution](./content/img/RevoSC_title.png)

#### 介绍

传送门：革命 是一个刚刚推出不久的优质传送门社区模组，其剧情优秀、玩法新颖（引入了很多原版废弃的测试机制）等特点立刻吸引了不少传送门系列的新老玩家们。

但美中不足的是其并没有支持中文游戏字幕，毫无疑问这加大了玩家们对理解剧情所需的难度。

所以我们花了些两天时间肝制出了这份包含主界面汉化在内的中文补丁。希望能借此填补官方所缺失的中文体验（目前仅支持简体中文哦）。

#### 下载

# **注：最新构建包存放位置已迁移至 [PortalRevo SC release](https://gitlab.com/ALexZerkio/PortalRevo_SC-release)。**

如果您从未使用过 GitLab 等代码托管网站。请按如下操作下载补丁包（以目前最新 Release 示范）：

![Step 1](./content/img/d1.png)

![Step 2](./content/img/d2.png)

#### 安装

**注：最新构建包存放位置已迁移至 [PortalRevo SC release](https://gitlab.com/ALexZerkio/PortalRevo_SC-release)。**

1. 下载或 git clone 最新构建版本的补丁包到您的计算机。如何下载见上图。

![Download](./content/img/d3.png)

2. 解压 PortalRevo_SC_latest.tar。

![](./content/img/tar.png)

3. 在 Steam - Portal Revolution - 浏览本地文件，打开游戏根目录。

![Game dir](./content/img/homedir.png)

4. 将“revolution”目录替换至 Portal Revolution 游戏根目录（包含了原版英文字幕的源文件，如安装过旧版本一并替换即可）。

![Replace](./content/img/replace.png)

**注：240110 更新废弃了之前直接替换原英字幕的做法。您需要些额外的步骤。**

1. 在 Steam - Portal Revolution - 属性 中添加启动项命令“ -language schinese”。

![Step 1](./content/img/s1.png)

![Step 2](./content/img/s2.png)

![Step 3](./content/img/s3.png)

3. 进入游戏，到设置里打开“开发者模式”。

![Setting](./content/img/setting.png)

![Dev mode](./content/img/devmode.png)

4. 按下“~”键打开开发者控制台，并使用“cc_lang schinese”指令来切换到简中字幕。

![step 4](./content/img/s4_console.png)

5. 开始游戏，享受中文化的传送门: 革命。

---

#### 赞助

如果您觉得这做的还不错，不妨到 B站 关注一下我哦：[ILIZE_栗子 的个人空间](https://space.bilibili.com/431870933) （请不要吝啬您的硬币）。

我的爱发电链接: https://afdian.net/a/485107C （未成年人禁止打赏）。
